<?php

namespace WarehouseX\Logistics\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * LogisticsProvider.
 */
class Provider extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $userId = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var string|null
     */
    public $createTime = null;

    /**
     * @var string|null
     */
    public $updateTime = null;
}

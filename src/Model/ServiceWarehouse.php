<?php

namespace WarehouseX\Logistics\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * LogisticsServiceWarehouse.
 */
class ServiceWarehouse extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $warehouseId = null;

    /**
     * @var string|null
     */
    public $service = null;
}

<?php

namespace WarehouseX\Logistics\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * LogisticsProviderAccountConfig.
 */
class ProviderAccountConfig extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string|null
     */
    public $key = null;

    /**
     * @var string|null
     */
    public $value = null;

    /**
     * @var string|null
     */
    public $createTime = null;

    /**
     * @var string|null
     */
    public $providerAccount = null;
}

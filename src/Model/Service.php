<?php

namespace WarehouseX\Logistics\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * LogisticsService.
 */
class Service extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $userId = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $code = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var string|null
     */
    public $createTime = null;

    /**
     * @var string|null
     */
    public $updateTime = null;

    /**
     * @var string|null
     */
    public $provider = null;

    /**
     * @var string|null
     */
    public $providerAccount = null;
}

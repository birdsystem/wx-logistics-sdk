<?php

namespace WarehouseX\Logistics\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * LogisticsProviderAccount.
 */
class ProviderAccount extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $companyName = null;

    /**
     * @var string|null
     */
    public $contactName = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string|null
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string|null
     */
    public $postCode = null;

    /**
     * @var string|null
     */
    public $countryIso = null;

    /**
     * @var string|null
     */
    public $pickupContact = null;

    /**
     * @var string|null
     */
    public $pickupBusinessName = null;

    /**
     * @var string|null
     */
    public $pickupAddressLine1 = null;

    /**
     * @var string|null
     */
    public $pickupAddressLine2 = null;

    /**
     * @var string|null
     */
    public $pickupAddressLine3 = null;

    /**
     * @var string|null
     */
    public $pickupCity = null;

    /**
     * @var string|null
     */
    public $pickupCounty = null;

    /**
     * @var string|null
     */
    public $pickupPostCode = null;

    /**
     * @var string|null
     */
    public $pickupCountryIso = null;

    /**
     * @var string|null
     */
    public $pickupEmail = null;

    /**
     * @var string|null
     */
    public $pickupTelephone = null;

    /**
     * @var string|null
     */
    public $createTime = null;

    /**
     * @var string|null
     */
    public $provider = null;
}

<?php

namespace WarehouseX\Logistics\Api;

use WarehouseX\Logistics\Model\ProviderAccountConfig as ProviderAccountConfigModel;

class ProviderAccountConfig extends AbstractAPI
{
    /**
     * Retrieves the collection of ProviderAccountConfig resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ProviderAccountConfigModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getProviderAccountConfigCollection',
        'GET',
        'api/logistics/provider_account_configs',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ProviderAccountConfig resource.
     *
     * @param ProviderAccountConfigModel $Model The new ProviderAccountConfig resource
     *
     * @return ProviderAccountConfigModel
     */
    public function postCollection(ProviderAccountConfigModel $Model): ProviderAccountConfigModel
    {
        return $this->request(
        'postProviderAccountConfigCollection',
        'POST',
        'api/logistics/provider_account_configs',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ProviderAccountConfig resource.
     *
     * @param string $id Resource identifier
     *
     * @return ProviderAccountConfigModel|null
     */
    public function getItem(string $id): ?ProviderAccountConfigModel
    {
        return $this->request(
        'getProviderAccountConfigItem',
        'GET',
        "api/logistics/provider_account_configs/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ProviderAccountConfig resource.
     *
     * @param string                     $id    Resource identifier
     * @param ProviderAccountConfigModel $Model The updated ProviderAccountConfig
     *                                          resource
     *
     * @return ProviderAccountConfigModel
     */
    public function putItem(string $id, ProviderAccountConfigModel $Model): ProviderAccountConfigModel
    {
        return $this->request(
        'putProviderAccountConfigItem',
        'PUT',
        "api/logistics/provider_account_configs/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ProviderAccountConfig resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteProviderAccountConfigItem',
        'DELETE',
        "api/logistics/provider_account_configs/$id",
        null,
        [],
        []
        );
    }
}

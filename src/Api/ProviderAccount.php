<?php

namespace WarehouseX\Logistics\Api;

use WarehouseX\Logistics\Model\ProviderAccount as ProviderAccountModel;

class ProviderAccount extends AbstractAPI
{
    /**
     * Retrieves the collection of ProviderAccount resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'name'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[name]'	string
     *                       'order[createTime]'	string
     *
     * @return ProviderAccountModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getProviderAccountCollection',
        'GET',
        'api/logistics/provider_accounts',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ProviderAccount resource.
     *
     * @param ProviderAccountModel $Model The new ProviderAccount resource
     *
     * @return ProviderAccountModel
     */
    public function postCollection(ProviderAccountModel $Model): ProviderAccountModel
    {
        return $this->request(
        'postProviderAccountCollection',
        'POST',
        'api/logistics/provider_accounts',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ProviderAccount resource.
     *
     * @param string $id Resource identifier
     *
     * @return ProviderAccountModel|null
     */
    public function getItem(string $id): ?ProviderAccountModel
    {
        return $this->request(
        'getProviderAccountItem',
        'GET',
        "api/logistics/provider_accounts/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ProviderAccount resource.
     *
     * @param string               $id    Resource identifier
     * @param ProviderAccountModel $Model The updated ProviderAccount resource
     *
     * @return ProviderAccountModel
     */
    public function putItem(string $id, ProviderAccountModel $Model): ProviderAccountModel
    {
        return $this->request(
        'putProviderAccountItem',
        'PUT',
        "api/logistics/provider_accounts/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ProviderAccount resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteProviderAccountItem',
        'DELETE',
        "api/logistics/provider_accounts/$id",
        null,
        [],
        []
        );
    }
}

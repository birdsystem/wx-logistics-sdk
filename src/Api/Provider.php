<?php

namespace WarehouseX\Logistics\Api;

use WarehouseX\Logistics\Model\Provider as ProviderModel;

class Provider extends AbstractAPI
{
    /**
     * Retrieves the collection of Provider resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'name'	string
     *                       'code'	string
     *                       'note'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'userId'	integer
     *                       'userId[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[name]'	string
     *                       'order[code]'	string
     *                       'order[note]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return ProviderModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getProviderCollection',
        'GET',
        'api/logistics/providers',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Provider resource.
     *
     * @param ProviderModel $Model The new Provider resource
     *
     * @return ProviderModel
     */
    public function postCollection(ProviderModel $Model): ProviderModel
    {
        return $this->request(
        'postProviderCollection',
        'POST',
        'api/logistics/providers',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Provider resource.
     *
     * @param string $id Resource identifier
     *
     * @return ProviderModel|null
     */
    public function getItem(string $id): ?ProviderModel
    {
        return $this->request(
        'getProviderItem',
        'GET',
        "api/logistics/providers/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Provider resource.
     *
     * @param string        $id    Resource identifier
     * @param ProviderModel $Model The updated Provider resource
     *
     * @return ProviderModel
     */
    public function putItem(string $id, ProviderModel $Model): ProviderModel
    {
        return $this->request(
        'putProviderItem',
        'PUT',
        "api/logistics/providers/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Provider resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteProviderItem',
        'DELETE',
        "api/logistics/providers/$id",
        null,
        [],
        []
        );
    }
}

<?php

namespace WarehouseX\Logistics\Api;

use WarehouseX\Logistics\Model\Service as ServiceModel;

class Service extends AbstractAPI
{
    /**
     * Retrieves the collection of Service resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'name'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[name]'	string
     *                       'order[createTime]'	string
     *
     * @return ServiceModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getServiceCollection',
        'GET',
        'api/logistics/services',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Service resource.
     *
     * @param ServiceModel $Model The new Service resource
     *
     * @return ServiceModel
     */
    public function postCollection(ServiceModel $Model): ServiceModel
    {
        return $this->request(
        'postServiceCollection',
        'POST',
        'api/logistics/services',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Service resource.
     *
     * @param string $id Resource identifier
     *
     * @return ServiceModel|null
     */
    public function getItem(string $id): ?ServiceModel
    {
        return $this->request(
        'getServiceItem',
        'GET',
        "api/logistics/services/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Service resource.
     *
     * @param string       $id    Resource identifier
     * @param ServiceModel $Model The updated Service resource
     *
     * @return ServiceModel
     */
    public function putItem(string $id, ServiceModel $Model): ServiceModel
    {
        return $this->request(
        'putServiceItem',
        'PUT',
        "api/logistics/services/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Service resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteServiceItem',
        'DELETE',
        "api/logistics/services/$id",
        null,
        [],
        []
        );
    }
}

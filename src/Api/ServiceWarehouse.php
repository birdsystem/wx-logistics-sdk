<?php

namespace WarehouseX\Logistics\Api;

use WarehouseX\Logistics\Model\ServiceWarehouse as ServiceWarehouseModel;

class ServiceWarehouse extends AbstractAPI
{
    /**
     * Retrieves the collection of ServiceWarehouse resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *
     * @return ServiceWarehouseModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getServiceWarehouseCollection',
        'GET',
        'api/logistics/service_warehouses',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ServiceWarehouse resource.
     *
     * @param ServiceWarehouseModel $Model The new ServiceWarehouse resource
     *
     * @return ServiceWarehouseModel
     */
    public function postCollection(ServiceWarehouseModel $Model): ServiceWarehouseModel
    {
        return $this->request(
        'postServiceWarehouseCollection',
        'POST',
        'api/logistics/service_warehouses',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ServiceWarehouse resource.
     *
     * @param string $id Resource identifier
     *
     * @return ServiceWarehouseModel|null
     */
    public function getItem(string $id): ?ServiceWarehouseModel
    {
        return $this->request(
        'getServiceWarehouseItem',
        'GET',
        "api/logistics/service_warehouses/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ServiceWarehouse resource.
     *
     * @param string                $id    Resource identifier
     * @param ServiceWarehouseModel $Model The updated ServiceWarehouse resource
     *
     * @return ServiceWarehouseModel
     */
    public function putItem(string $id, ServiceWarehouseModel $Model): ServiceWarehouseModel
    {
        return $this->request(
        'putServiceWarehouseItem',
        'PUT',
        "api/logistics/service_warehouses/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ServiceWarehouse resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteServiceWarehouseItem',
        'DELETE',
        "api/logistics/service_warehouses/$id",
        null,
        [],
        []
        );
    }
}

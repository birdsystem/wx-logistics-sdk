<?php

namespace WarehouseX\Logistics;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getProviderAccountConfigCollection' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\ProviderAccountConfig[]',
        ],
        'postProviderAccountConfigCollection' => [
            '201.' => 'WarehouseX\\Logistics\\Model\\ProviderAccountConfig',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getProviderAccountConfigItem' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\ProviderAccountConfig',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putProviderAccountConfigItem' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\ProviderAccountConfig',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteProviderAccountConfigItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getProviderAccountCollection' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\ProviderAccount[]',
        ],
        'postProviderAccountCollection' => [
            '201.' => 'WarehouseX\\Logistics\\Model\\ProviderAccount',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getProviderAccountItem' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\ProviderAccount',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putProviderAccountItem' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\ProviderAccount',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteProviderAccountItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getProviderCollection' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\Provider[]',
        ],
        'postProviderCollection' => [
            '201.' => 'WarehouseX\\Logistics\\Model\\Provider',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getProviderItem' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\Provider',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putProviderItem' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\Provider',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteProviderItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getServiceWarehouseCollection' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\ServiceWarehouse[]',
        ],
        'postServiceWarehouseCollection' => [
            '201.' => 'WarehouseX\\Logistics\\Model\\ServiceWarehouse',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getServiceWarehouseItem' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\ServiceWarehouse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putServiceWarehouseItem' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\ServiceWarehouse',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteServiceWarehouseItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getServiceCollection' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\Service[]',
        ],
        'postServiceCollection' => [
            '201.' => 'WarehouseX\\Logistics\\Model\\Service',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getServiceItem' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\Service',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putServiceItem' => [
            '200.' => 'WarehouseX\\Logistics\\Model\\Service',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteServiceItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
